package uz.ort.api.routers;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class MainApiRouter {

    public Router createRouting(Vertx vertx) {

        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route("/version").handler(this::getVersion);
        return router;
    }

    private void getVersion(RoutingContext context) {
        context.response().end("{\"ver\":\"1.0.0\"}");
    }


}
