package uz.ort.api.database;

public class QueryDemo extends AbstractQuery {
    public QueryDemo() {
        GET_LIST = "" +
                " SELECT id, name, intval " +
                "   FROM public.demos " +
                "  WHERE status = 'A'; ";
        GET_BY_ID = "" +
                " SELECT id, name, intval " +
                "   FROM public.demos " +
                "  WHERE status = 'A'" +
                "    AND id = ? ; ";
        INSERT = "" +
                " INSERT INTO public.demos" +
                "        (name, intval)" +
                " VALUES (?, ?); ";
        UPDATE = "" +
                " UPDATE public.demos " +
                "   SET name = ?, intval = ?, mod_date = now() " +
                " WHERE id = ? " +
                "   AND status = 'A';";
        DELETE = "" +
                " UPDATE public.demos " +
                "   SET status = 'D', exp_date = now() " +
                " WHERE id = ? " +
                "   AND status = 'A';";
    }
}
