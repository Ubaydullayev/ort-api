package uz.ort.api.controller;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstructContoller {

    private static final Logger LOGGER = LoggerFactory.getLogger("AbstructContoller");

    protected Vertx vertx;

    AbstructContoller(Vertx vertx) {
        this.vertx = vertx;
    }

    public JsonObject getParams(RoutingContext context) {
        JsonObject params;
        try {
            params = context.getBodyAsJson();
        } catch (Exception e) {
            params = new JsonObject();
        }
        return params;
    }

}
